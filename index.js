const Day    = require('./lib/Day'),
      Month  = require('./lib/Month'),
      Error  = require('./lib/Error'),
      Action = require('./lib/action');

module.exports = {
    Error:     Error,
    run:       function (options) {
        return Action.run(options);
    },
    configure: function (config) {
        Day.setWorkTime(config.get('day.worktime'));
        Month.setDataDir(config.get('app.data.dir'));
    }
};
