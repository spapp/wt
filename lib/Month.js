const Util           = require('util'),
      FileSystem     = require('fs'),
      Path           = require('path'),
      Day            = require('./Day'),
      FIELD_WORKTIME = 'WORKTIME',
      FIELD_DAY_OFF  = Day.FIELD_DAY_OFF,
      GLUE_LINE      = '\n';

let DATA_DIR = '';

/**
 * @typedef WtDate
 * @property {Date} date
 * @property {function(): number} year
 * @property {function(): number} month
 * @property {function(): number} day
 * @property {function(format:string): string} toString
 */

/**
 * @class
 * @type {Month}
 */
module.exports = class Month {

    static get FIELD_WORKTIME() {
        return FIELD_WORKTIME;
    }

    static get FIELD_DAY_OFF() {
        return FIELD_DAY_OFF;
    }

    /**
     * @constructor
     * @param {string|Day[]} data
     */
    constructor(data) {
        this.data = 'string' === typeof data ? parse(data) : data;
    }

    /**
     * Creates a blank month
     * @static
     * @param {number} year
     * @param {number} month
     * @returns {Month}
     */
    static create(year, month) {
        return new Month(createMonth(year, month));
    }

    /**
     * Reads data from a file
     * @static
     * @param {number} year
     * @param {number} month
     * @returns {Month}
     */
    static fromFile(year, month) {
        let fileName = Util.format('%s-%s', year, String(month).padStart(2, '0')),
            file     = Path.join(DATA_DIR, fileName);

        return new Month(FileSystem.readFileSync(file).toString());
    }

    /**
     * Sets data directory
     * @static
     * @param {string} dataDir
     */
    static setDataDir(dataDir) {
        DATA_DIR = dataDir;
    }

    /**
     * Returns month as a string
     * @returns {string}
     */
    toString() {
        let text = [Day.headerToString()];

        for (let i = 0; i < this.data.length; i++) {
            text.push(this.data[i].toString());
        }

        return text.join('\n');
    }

    /**
     * Save data into the data file
     */
    save() {
        let date     = new Date(this.find(1).get(Day.FIELD_DATE)),
            year     = date.getFullYear(),
            month    = date.getMonth() + 1,
            fileName = Util.format('%s-%s', year, String(month).padStart(2, '0')),
            file     = Path.join(DATA_DIR, fileName);

        return FileSystem.writeFileSync(file, this.toString());
    }

    /**
     * Returns a day data
     * @param {number} day
     * @returns {Day}
     */
    find(day) {
        return this.data[day - 1];
    }

    getSum() {
        let days     = this.data,
            worktime = 0,
            off      = [];

        for (let day of days) {
            if (Day.VALUE_TRUE === day.get(Day.FIELD_DAY_OFF)) {
                off.push(Date.format('d', new Date(day.get(Day.FIELD_DATE))));
            } else {
                worktime += (day.getSum() || 0);
            }
        }

        return {[FIELD_WORKTIME]: worktime, [FIELD_DAY_OFF]: off};
    }

    /**
     * Returns date of month
     * @returns {WtDate}
     */
    getDate() {
        let day = this.find(1);

        return {
            date:     new Date(day.get(Day.FIELD_DATE)),
            toString: function (format) {
                return Date.format(format ? format : 'yyyy MMMM', this.date);
            },
            year:     function () {
                return this.date.getFullYear();
            },
            month:    function () {
                return this.date.getMonth() + 1;
            },
            day:      function () {
                return this.date.getDate();
            }
        };
    }
};

/**
 * Parses data text
 * @private
 * @param {string} text
 * @returns {Day[]}
 */
function parse(text) {
    let data   = text.split(GLUE_LINE),
        values = [];

    data.shift();

    while (data.length > 0) {
        let line = (data.shift() || '').trim();

        if (line) {
            values.push(new Day(line));
        }
    }


    return values;
}

/**
 * Returns a blank month
 * @private
 * @param {number} year
 * @param {number} month
 * @returns {Day[]}
 */
function createMonth(year, month) {
    let date = new Date(Util.format('%s-%s-01', year, String(month).padStart(2, '0'))),
        data = [];

    for (let i = 1; i <= 31; i++) {
        date.setDate(i);

        if (month !== date.getMonth() + 1) {
            break;
        }

        data.push(Day.create(date));
    }

    return data;
}