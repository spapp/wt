const Os                      = require('os'),
      EOL                     = Os.EOL,
      DEFAULT_CAPTION         = '',
      DEFAULT_COLUMN_WIDTH    = 20,
      SEPARATOR_BORDER_LEFT   = '| ',
      SEPARATOR_BORDER_RIGHT  = ' |',
      SEPARATOR_BORDER_MIDDLE = ' | ',
      SEPARATOR_LEFT          = ' ',
      SEPARATOR_RIGHT         = ' ',
      SEPARATOR_MIDDLE        = '  ',
      PAD_TEXT_LINE           = '-',
      DEFAULT_FORMATTER       = (value) => value;

/**
 * @class
 * @type {Table}
 */
module.exports = class Table {
    /**
     * @constructor
     * @param {string[]} columns
     * @param {string} caption
     * @param {number[]} columnWidth
     */
    constructor(columns, caption, columnWidth) {
        this.columns     = columns;
        this.caption     = caption || DEFAULT_CAPTION;
        this.columnWidth = [].concat((columnWidth || []));
        this.data        = [];
        this.footer      = [];
        this.hasBorder   = true;
        this.hasHeader   = true;
        this.formatter   = DEFAULT_FORMATTER;
    }

    /**
     * Appends a new row to table
     * @param {*[]} row
     * @returns {Table}
     */
    append(row) {
        this.data.push(row);

        return this;
    }

    /**
     * Appends a new footer row to table
     * @param {*[]} row
     * @returns {Table}
     */
    appendFooter(row) {
        this.footer.push(row);

        return this;
    }

    /**
     * Adds cell formatter method
     *
     * @param {Function} formatter
     * @returns {Table}
     */
    addFormatter(formatter) {
        this.formatter = formatter;

        return this;
    }

    /**
     * Sets border
     *
     * @param {boolean} hasBorder
     * @returns {Table}
     */
    withBorder(hasBorder) {
        this.hasBorder = !!hasBorder;

        return this;
    }

    /**
     * @param {boolean} hasHeader
     * @returns {Table}
     */
    withHeader(hasHeader) {
        this.hasHeader = !!hasHeader;

        return this;
    }

    /**
     * Returns table as a string
     *
     * @returns {string}
     */
    toString() {
        let me      = this,
            data    = me.data,
            footer  = me.footer,
            caption = me.caption ? me.formatter(me.caption, null, getRowWidth.call(me), true) : '',
            table   = [];

        if (caption) {
            table.push(caption);
        }

        if (me.hasHeader) {
            table.push(getHeader.call(me));
        }

        if (data.length > 0) {
            for (const rowData of data) {
                table.push(getRow.call(me, rowData));
            }
        }

        if (this.hasBorder) {
            table.push(getHr.call(me, table[table.length - 1].length));
        }

        if (footer.length > 0) {
            for (const rowData of footer) {
                table.push(getRow.call(me, rowData, false, false));
            }
        }

        return table.join(EOL);
    }
};

/**
 * Returns column width
 * @private
 * @param {number} columnIndex
 * @returns {number}
 */
function getColumnWidth(columnIndex) {
    let columnWidth = DEFAULT_COLUMN_WIDTH;

    if (this.columnWidth && this.columnWidth.length - 1 >= columnIndex) {
        columnWidth = this.columnWidth[columnIndex];
    }

    if (this.columns[columnIndex].length > columnWidth) {
        columnWidth = this.columns[columnIndex].length;
    }

    return columnWidth;
}

/**
 * Returns row width
 * @private
 * @returns {number}
 */
function getRowWidth() {
    if (!this.rowWidth) {
        let columns  = this.columns,
            rowWidth = 0;

        for (let i = 0; i < columns.length; i++) {
            rowWidth += getColumnWidth.call(this, i);
        }

        if (this.hasBorder) {
            rowWidth += SEPARATOR_LEFT.length;
            rowWidth += SEPARATOR_MIDDLE.length * (columns.length - 1);
            rowWidth += SEPARATOR_RIGHT.length;
        }

        this.rowWidth = rowWidth;
    }

    return this.rowWidth;
}

/**
 * Returns row as a string
 * @private
 * @param {object} data
 * @param {number} align
 * @param {boolean} isHeader
 * @param {boolean} withBorder
 * @returns {string}
 */
function getRow(data, isHeader, withBorder) {
    let row             = [],
        columns         = this.columns,
        formatter       = this.formatter,
        hasBorder       = 'boolean' === typeof withBorder ? withBorder : this.hasBorder,
        middleSeparator = hasBorder ? SEPARATOR_MIDDLE : '  ',
        leftSeparator   = hasBorder ? SEPARATOR_LEFT : ' ',
        rightSeparator  = hasBorder ? SEPARATOR_RIGHT : ' ';

    if ('boolean' === typeof withBorder && this.hasBorder && !withBorder) {
        middleSeparator = ''.padBoth(SEPARATOR_MIDDLE.length);
        leftSeparator   = ''.padBoth(SEPARATOR_LEFT.length);
        rightSeparator  = ''.padBoth(SEPARATOR_RIGHT.length);
    }

    row.push(leftSeparator);

    for (let i = 0; i < data.length; i++) {
        let columnWidth = getColumnWidth.call(this, i),
            fieldName   = columns[i],
            value       = data[i];

        if (i > 0) {
            row.push(middleSeparator);
        }

        row.push(formatter(value, fieldName, columnWidth, isHeader, data));
    }

    row.push(rightSeparator);

    return row.join('');
}

/**
 * Returns header as a string
 * @private
 * @returns {string}
 */
function getHeader() {
    let me        = this,
        header    = [],
        hasBorder = me.hasBorder,
        headerRow = getRow.call(me, me.columns, true);

    if (hasBorder) {
        header.push(getHr.call(me));
    }

    header.push(headerRow);

    if (hasBorder) {
        header.push(getHr.call(me));
    }

    return header.join(EOL);
}

/**
 * Returns a row separator line
 * @private
 * @returns {string}
 */
function getHr(width) {
    width = width || getRowWidth.call(this);

    return ''.padStart(width, PAD_TEXT_LINE);
}
