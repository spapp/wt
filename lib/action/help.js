const FileSystem     = require('fs'),
      Path           = require('path'),
      MAN_FILE       = Path.resolve(Path.join(__dirname, '..', '..', 'bin', 'wt.man.html')),
      STYLE_MAP      = {
          'b': String.STYLE_BOLD,
          'i': String.STYLE_ITALIC,
          'u': String.STYLE_UNDERLINE
      },
      STYLE_PATTERN  = /<([a-z])>(.*)<\/[a-z]>/gm,
      STYLE_REPLACER = (m, style, text) => STYLE_MAP[style] ? text.toStyle(STYLE_MAP[style]) : text;

module.exports = {
    run: function (options) {
        let text = FileSystem.readFileSync(MAN_FILE).toString();

        return text
            .replace(STYLE_PATTERN, STYLE_REPLACER)
            .replace(STYLE_PATTERN, STYLE_REPLACER);
    }
};
