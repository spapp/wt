const Util        = require('util'),
      Month       = require('../Month'),
      Day         = require('../Day'),
      Table       = require('../Table'),
      DAY_OFF_YES = 1,
      DAY_OFF_NO  = 0;

module.exports = {
    /**
     * @param {Config} options
     * @returns {Table}
     */
    run: function (options) {
        let [y, m, d] = getDate(options),
            isVerbose = options.get('verbose', false),
            data      = getFieldValues(options),
            fields    = Object.keys(data),
            date      = Date.format('MMMM dd, yyyy', new Date(y, m - 1, d)),
            caption   = Util.format('Update finished (%s)', date),
            table     = new Table(['Field', 'Value'], caption),
            month, day;

        try {
            month = Month.fromFile(y, m);
        } catch (error) {
            if ('ENOENT' === error.code) {
                month = Month.create(y, m);
            } else {
                throw error;
            }
        }

        day = month.find(d);
        table.addFormatter(cellFormatter);

        for (const field of fields) {
            if ('undefined' !== typeof data[field]) {
                day.set(field, data[field]);

                if (isVerbose) {
                    table.append([field, day.get(field)]);
                }
            }
        }

        month.save();

        return isVerbose ? table : caption;
    }
};

function cellFormatter(value, fieldName, columnWidth, isHeader, data) {
    value = String(value);

    if ('Field' === fieldName) {
        value = value.humanize();
    } else if ('Value' === fieldName && Day.FIELD_DAY_OFF === data[0]) {
        value = String(DAY_OFF_YES) === String(value) ? 'Yes' : 'No';
    }

    if (value.length > columnWidth) {
        value = value.truncate(columnWidth);
    }

    if (isHeader) {
        value = value.humanize().padBoth(columnWidth).toStyle(String.STYLE_BOLD);
    } else {
        value = value.padEnd(columnWidth);
    }

    return value;
}

/**
 * @private
 * @param {Config} options
 * @returns {number[]}
 */
function getDate(options) {
    let now = new Date(),
        y   = options.get('year', now.getFullYear()),
        m   = options.get('month', now.getMonth() + 1),
        d   = options.get('day', now.getDate());

    return [y, m, d];
}

/**
 * @private
 * @param {Config} options
 * @returns {{}}
 */
function getFieldValues(options) {
    let dayOff = options.get('off');

    if (dayOff) {
        dayOff = ['yes', 'y', 'true', '1'].indexOf(String(dayOff).toLowerCase()) > -1 ? DAY_OFF_YES : DAY_OFF_NO;
    }

    return {
        [Day.FIELD_START_TIME]:  options.get('start-time'),
        [Day.FIELD_END_TIME]:    options.get('end-time'),
        [Day.FIELD_LUNCH_TIME]:  options.get('lunch'),
        [Day.FIELD_ABSENCE]:     options.get('absence'),
        [Day.FIELD_DAY_OFF]:     dayOff,
        [Day.FIELD_DESCRIPTION]: options.get('description')
    };
}
