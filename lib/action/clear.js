const Util  = require('util'),
      Month = require('../Month'),
      Day   = require('../Day'),
      Table = require('../Table');

module.exports = {
    /**
     * @param {Config} options
     * @returns {Table|string}
     */
    run: function (options) {
        let y     = options.get('year'),
            m     = options.get('month'),
            d     = options.get('day'),
            date  = Date.format('MMMM dd, yyyy', new Date(y, m - 1, d)),
            month = Month.fromFile(y, m),
            day   = month.find(d);

        day.set(Day.FIELD_START_TIME, '');
        day.set(Day.FIELD_END_TIME, '');
        day.set(Day.FIELD_LUNCH_TIME, '');
        day.set(Day.FIELD_ABSENCE, '');
        day.set(Day.FIELD_DAY_OFF, '');
        day.set(Day.FIELD_DESCRIPTION, '');

        month.save();

        return Util.format('Clear is finished (%s)', date);
    }
};
