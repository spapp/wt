const Util         = require('util'),
      FileSystem   = require('fs'),
      Path         = require('path'),
      FIELD_ACTION = 'action';

module.exports = {
    /**
     * @type {string}
     * @constant
     */
    get FIELD_ACTION() {
        return FIELD_ACTION;
    },

    /**
     * Executes an action
     *
     * If the `action` is not valid then will throw an error
     *
     * @param {Config} options
     */
    run: function (options) {
        let action = options.get(FIELD_ACTION),
            file   = Path.resolve(__dirname, action + '.js');

        if (!FileSystem.existsSync(file)) {
            throw new Error(Util.format('The "%s" action is not supported.', action));
        }

        return require(file).run(options);
    }
};

