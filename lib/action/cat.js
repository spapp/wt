const Month = require('../Month');

/**
 * @typedef CatOptions
 * @property {number} y
 * @property {number} year
 * @property {number} m
 * @property {number} month
 */

module.exports = {
    /**
     * @param {Config} options
     * @returns {Table}
     */
    run: function (options) {
        let now = new Date(),
            y   = options.get('year', now.getFullYear()),
            m   = options.get('month', now.getMonth() + 1);

        return Month.fromFile(y, m);
    }
};
