const Month      = require('../Month'),
      TableMonth = require('../Table/Month'),
      TableDay   = require('../Table/Day'),
      TableYear  = require('../Table/Year');

module.exports = {
    run: function (options) {
        let now = new Date(),
            y   = options.get('year', now.getFullYear()),
            m   = options.get('month', 0),
            d   = options.get('day', 0),
            report;

        if (d) {
            m      = m || now.getMonth() + 1;
            report = getDayReport(y, m, d);
        } else if (m) {
            report = getMonthReport(y, m);
        } else {
            report = getYearReport(y);
        }

        return report;
    }
};

function getDayReport(y, m, d) {
    let month = Month.fromFile(y, m);

    return new TableDay(month.find(d));
}

function getMonthReport(y, m) {
    let response;

    try {
        response = new TableMonth(Month.fromFile(y, m));
    } catch (error) {
        if ('ENOENT' === error.code) {
            response = 'There is not report for ' + y + '-' + m;
        } else {
            throw error;
        }
    }

    return response;
}

function getYearReport(y) {
    let months = [];

    for (let m = 1; m < 13; m++) {
        try {
            months.push(Month.fromFile(y, m));
        } catch (error) {

        }
    }

    return new TableYear(months);
}
