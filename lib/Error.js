const Os = require('os');

module.exports = class WtError extends Error {
    constructor(error, isDebug) {
        super();

        this.error   = error;
        this.isDebug = isDebug;
    }

    toString() {
        let output = [
            'ERROR'.toStyle(String.STYLE_COLOR_HIGH_RED),
            this.error.message.toStyle(String.STYLE_COLOR_RED)
        ];

        if (true === this.isDebug) {
            output.push(this.error.stack);
            output = output.join(Os.EOL);
        } else {
            output = output.join(': ');
        }

        return output;
    }
};
