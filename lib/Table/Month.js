const Table       = require('../Table'),
      Month       = require('../Month'),
      Day         = require('../Day'),
      FORMAT_DATE = 'MMM d, EEE',
      FIELD_SUM   = '';
/**
 * @class
 * @type {TableMonth}
 */
module.exports = class TableMonth extends Table {

    /**
     * @constructor
     * @param {Month} month
     */
    constructor(month) {
        let date        = month.getDate(),
            columns     = Day.getHeaderFields(),
            caption     = date.toString('MMMM, yyyy'),
            columnWidth = [12, 8, 8, 3, 3, 1, 30, 10];

        columns.push(FIELD_SUM);

        super(columns, caption, columnWidth);

        this.month = month;

        this.addFormatter(cellFormatter);
    }

    /**
     * @inheritDoc
     * @returns {string}
     */
    toString() {
        let month = this.month,
            sum   = 0;

        for (let i = 0; i <= 31; i++) {
            let day = month.find(i);

            if (day) {
                let data        = day.toJson();
                data[FIELD_SUM] = parseInt(day.getSum() ? day.getSum() / 60 : 0, 10);
                sum += data[FIELD_SUM];

                this.append(Object.values(data));
            }
        }

        this.appendFooter(['', '', '', '', '', '', '', sum]);

        return super.toString();
    }
};

/**
 * Table cell formatter
 * @private
 * @param {string} value
 * @param {string} fieldName
 * @param {number} columnWidth
 * @param {boolean} isHeader
 * @param {array} data
 * @returns {string}
 */
function cellFormatter(value, fieldName, columnWidth, isHeader, data) {
    if (isHeader) {
        value = value.humanize();
    } else if (Day.FIELD_DATE === fieldName && value) {
        value = Date.format(FORMAT_DATE, new Date(value));
    } else if (Day.FIELD_DAY_OFF === fieldName) {
        value = '1' === value ? 'Yes' : '';
    } else if (FIELD_SUM === fieldName) {
        value = 0 !== value ? (new Date.Period(value * 60)).format('%hh %mm') : '';
    }

    if (value.length > columnWidth) {
        value = value.truncate(columnWidth);
    }

    value = isHeader ? value.padBoth(columnWidth) : value.padEnd(columnWidth);

    return isHeader ? value.toStyle(String.STYLE_BOLD) : value;
}
