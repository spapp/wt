const Table       = require('../Table'),
      Day         = require('../Day'),
      FORMAT_DATE = 'EEEE, MMMM d, yyyy',
      FIELD_FIELD = 'Field',
      FIELD_VALUE = 'Value';
/**
 * @class
 * @type {DayMonth}
 */
module.exports = class DayMonth extends Table {

    /**
     * @constructor
     * @param {Day} day
     */
    constructor(day) {
        let columns     = [FIELD_FIELD, FIELD_VALUE],
            columnWidth = [12, 40];

        super(columns, null, columnWidth);

        this.day = day;

        this.addFormatter(cellFormatter);
        this.withHeader(false);
    }

    /**
     * @inheritDoc
     * @returns {string}
     */
    toString() {
        let day    = this.day,
            fields = Day.getHeaderFields();

        for (const field of fields) {
            this.append([field, day.get(field)]);
        }

        this.appendFooter(['', (new Date.Period(day.getSum() || 0)).format('%hh %mm')]);

        return super.toString();
    }
};

/**
 * Table cell formatter
 * @private
 * @param {string} value
 * @param {string} fieldName
 * @param {number} columnWidth
 * @param {boolean} isHeader
 * @param {array} data
 * @returns {string}
 */
function cellFormatter(value, fieldName, columnWidth, isHeader, data) {
    if (FIELD_FIELD === fieldName) {
        value = value.humanize();
    } else if (FIELD_VALUE === fieldName) {
        if (Day.FIELD_LUNCH_TIME === data[0] || Day.FIELD_ABSENCE === data[0]) {
            value = (new Date.Period(value * 60)).format('%hh %mm');
        } else if (Day.FIELD_DATE === data[0]) {
            value = Date.format(FORMAT_DATE, new Date(value));
        } else if (Day.FIELD_DAY_OFF === data[0]) {
            value = Day.VALUE_TRUE === value ? 'Yes' : '';
        }
    }

    if (value.length > columnWidth) {
        value = value.truncate(columnWidth);
    }

    value = isHeader ? value.padBoth(columnWidth) : value.padEnd(columnWidth);

    if (FIELD_FIELD === fieldName) {
        value = value.toStyle(String.STYLE_BOLD);
    } else if (Day.FIELD_DATE === data[0]) {
        value = value.toStyle(String.STYLE_INVERTED);
    }

    return value;
}
