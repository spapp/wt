const Util        = require('util'),
      Table       = require('../Table'),
      Month       = require('../Month'),
      FIELD_MONTH = 'MONTH';

/**
 * @class
 * @type {YearMonth}
 */
module.exports = class YearMonth extends Table {

    /**
     * @constructor
     * @param {Month[]} months
     */
    constructor(months) {
        let columns     = [FIELD_MONTH, Month.FIELD_WORKTIME, Month.FIELD_DAY_OFF],
            columnWidth = [12, 10, 30];

        super(columns, null, columnWidth);

        this.months = months;

        this.addFormatter(cellFormatter);
    }

    /**
     *                 2020
     * --------------------------------------
     *   Month     WorkTime        DayOff
     * --------------------------------------
     * January   -20m            2 (11, 21)
     * February  12m
     * ...
     * December  1h 20m          3 (1, 4, 5)
     * --------------------------------------
     *           1h 12m          5
     *
     * @inheritDoc
     * @returns {string}
     */
    toString() {
        let me      = this,
            months  = me.months,
            yearSum = {[Month.FIELD_WORKTIME]: 0, [Month.FIELD_DAY_OFF]: 0};

        for (const month of months) {
            let sum  = month.getSum(),
                date = month.getDate();

            yearSum[Month.FIELD_WORKTIME] += sum[Month.FIELD_WORKTIME];
            yearSum[Month.FIELD_DAY_OFF] += sum[Month.FIELD_DAY_OFF].length;

            me.append([date.toString('MMMM'), (new Date.Period(sum[Month.FIELD_WORKTIME])).format('%hh %mm'), sum[Month.FIELD_DAY_OFF]]);

            if (!me.caption) {
                me.caption = date.year();
            }
        }

        me.appendFooter([
            '', (new Date.Period(yearSum[Month.FIELD_WORKTIME])).format('%hh %mm'), yearSum[Month.FIELD_DAY_OFF] || ''
        ]);

        return super.toString();
    }
};

/**
 * Table cell formatter
 * @private
 * @param {string} value
 * @param {string} fieldName
 * @param {number} columnWidth
 * @param {boolean} isHeader
 * @param {array} data
 * @returns {string}
 */
function cellFormatter(value, fieldName, columnWidth, isHeader, data) {
    value = String(value);

    if (FIELD_MONTH === fieldName) {
        value = value.toString('MMMM');
    }

    if (Month.FIELD_DAY_OFF === fieldName && Array.isArray(value)) {
        if (value.length) {
            value = Util.format('%s (%s)', value.length, value.join(', '));
        }
    }

    if (value.length > columnWidth) {
        value = value.truncate(columnWidth);
    }

    if (isHeader) {
        value = value.humanize().padBoth(columnWidth);
    } else {
        value = value.padEnd(columnWidth);
    }

    if (isHeader) {
        value = value.toStyle(String.STYLE_BOLD);
    }

    return value;
}
