const GLUE_VALUES       = ';',
      FIELD_DATE        = 'DATE',
      FIELD_START_TIME  = 'START_TIME',
      FIELD_END_TIME    = 'END_TIME',
      FIELD_LUNCH_TIME  = 'LUNCH_TIME',
      FIELD_ABSENCE     = 'ABSENCE',
      FIELD_DAY_OFF     = 'DAY_OFF',
      FIELD_DESCRIPTION = 'DESCRIPTION',
      FIELD_LINE        = [
          FIELD_DATE, FIELD_START_TIME, FIELD_END_TIME, FIELD_LUNCH_TIME,
          FIELD_ABSENCE, FIELD_DAY_OFF, FIELD_DESCRIPTION
      ],
      DATE_FORMAT       = 'yyyy-MM-dd',
      FORMAT_TIME       = 'HH:mm:ss',
      VALUE_TRUE        = '1';

let WORK_TIME = 8 * 60 * 60;

/**
 * @type Day
 * @class
 */
class Day {
    /**
     * @constructor
     * @param {string} [data='']
     */
    constructor(data = '') {
        this.data = {};

        if ('string' === typeof data) {
            data = data.split(GLUE_VALUES);
        }

        for (let i = 0; i < FIELD_LINE.length; i++) {
            this.data[FIELD_LINE[i]] = (data[i] ? data[i] : '');
        }
    }

    /**
     * Returns a blank day
     * @static
     * @param {Date|string} [date]
     * @returns {Day}
     */
    static create(date) {
        let instance = new Day();

        date = 'string' === typeof date ? new Date(date) : date;

        for (let i = 0; i < FIELD_LINE.length; i++) {
            instance.set(FIELD_LINE[i], '');
        }

        instance.set(FIELD_DATE, Date.format(DATE_FORMAT, date));

        return instance;
    }

    /**
     * Returns header fields
     * @static
     * @returns {string[]}
     */
    static getHeaderFields() {
        return [].concat(FIELD_LINE);
    }

    /**
     * Returns header fields as text
     * @static
     * @returns {string}
     */
    static headerToString() {
        return Day.getHeaderFields().join(GLUE_VALUES);
    }

    /**
     * Returns worktime on one day in seconds
     * @static
     * @returns {number}
     */
    static getWorkTime() {
        return WORK_TIME;
    }

    /**
     * Sets worktime
     * @static
     * @param {number} workTime
     */
    static setWorkTime(workTime) {
        WORK_TIME = workTime;
    }

    /**
     * Returns data as an objecct
     * @returns {{}}
     */
    toJson() {
        return Object.assign({}, this.data);
    }

    /**
     * @returns {number}
     */
    getSum() {
        let startTime = this.get(Day.FIELD_START_TIME),
            endTime   = this.get(Day.FIELD_END_TIME),
            time;

        if (startTime && endTime) {
            startTime = new Date('1970-01-01T' + startTime + '+00:00');
            endTime   = new Date('1970-01-01T' + endTime + '+00:00');
            time      = (endTime.getTime() - startTime.getTime() - Day.getWorkTime() * 1000) / 1000;
            time      = time - this.get(Day.FIELD_LUNCH_TIME, 0) * 60 - this.get(Day.FIELD_ABSENCE, 0) * 60;
        }

        return Math.round(time);
    }

    /**
     * Returns day data as a text
     * @returns {string}
     */
    toString() {
        let values = [];

        for (let i = 0; i < FIELD_LINE.length; i++) {
            values.push(this.get(FIELD_LINE[i]));
        }

        return values.join(GLUE_VALUES);
    }

    /**
     * Returns a field value
     * @param {string} name
     * @param {*} defaultValue
     * @returns {*}
     */
    get(name, defaultValue) {
        if ('undefined' !== typeof this.data[name]) {
            defaultValue = this.data[name];
        }

        return defaultValue;
    }

    /**
     * Set a field value
     * @param {string} name
     * @param {string} value
     */
    set(name, value) {
        if ((FIELD_START_TIME === name || FIELD_END_TIME === name) && '' !== value) {
            value = Date.format(FORMAT_TIME, 'now' === value ? new Date() : new Date('2000-01-01 ' + value));
        }

        this.data[name] = value;
    }
}

/**
 * @constant {string} FIELD_DATE
 * @memberOf Day
 */

/**
 * @constant {string} FIELD_START_TIME
 * @memberOf Day
 */

/**
 * @constant {string} FIELD_END_TIME
 * @memberOf Day
 */

/**
 * @constant {string} FIELD_LUNCH_TIME
 * @memberOf Day
 */

/**
 * @constant {string} FIELD_ABSENCE
 * @memberOf Day
 */

/**
 * @constant {string} FIELD_DAY_OFF
 * @memberOf Day
 */

/**
 * @constant {string} FIELD_DESCRIPTION
 * @memberOf Day
 */
for (let i = 0; i < FIELD_LINE.length; i++) {
    Day.__defineGetter__('FIELD_' + FIELD_LINE[i], () => ((i) => FIELD_LINE[i])(i));
}

Day.__defineGetter__('VALUE_TRUE', () => VALUE_TRUE);

module.exports = Day;
